var selected_home_blue = [];
var selected_home_red = [];
var bool = false;

function init_game() {
  document.getElementById("root").innerHTML = `
  `;
}

function blue_checked(id) {
  var table = document.getElementById("mytable").getElementsByTagName("td");
  selected_home_blue.push(id);
  table[id - 1].style.backgroundColor = "blue";
  check_game(selected_home_blue, "blue");
}

function red_checked(id) {
  var table = document.getElementById("mytable").getElementsByTagName("td");
  selected_home_red.push(id);
  table[id - 1].style.backgroundColor = "red";
  check_game(selected_home_red, "red");
}

function check_game(array, color) {
  if (
    (array.includes("1") && array.includes("2") && array.includes("3")) ||
    (array.includes("4") && array.includes("5") && array.includes("6")) ||
    (array.includes("7") && array.includes("8") && array.includes("9")) ||
    (array.includes("1") && array.includes("5") && array.includes("9")) ||
    (array.includes("7") && array.includes("5") && array.includes("3")) ||
    (array.includes("1") && array.includes("4") && array.includes("7")) ||
    (array.includes("2") && array.includes("5") && array.includes("8")) ||
    (array.includes("3") && array.includes("6") && array.includes("9"))
  ) {
    alert(color + "won the game!");
  }
}

function select_home(id) {
  alert(id);
}

function turn() {
  bool = !bool;
  return bool;
}

var table = document.querySelector("table");
// listen for a click
table.addEventListener("click", function (ev) {
  // get the event targets ID
  var serviceID = ev.target.id;
  turn() ? blue_checked(serviceID) : red_checked(serviceID);
});
